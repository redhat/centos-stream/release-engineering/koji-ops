---

# Externalrepos

- name: Create an external repo of signing tools 
  koji_external_repo:
    name: "c10s-signing"
    url: "http://infra-mirror.stream.rdu2.redhat.com/signing/el10/$arch/"

# RPM tags

- name: Create the buildtools tag
  koji_tag:
    name: c10s-build-tools
    groups:
      build:
        - bash
        - bzip2
        - centos-stream-release
        - centpkg-minimal
        - coreutils
        - cpio
        - diffutils
        - findutils
        - gawk
        - glibc-minimal-langpack
        - grep
        - gzip
        - info
        - patch
        - redhat-rpm-config
        - rpm-build
        - sed
        - shadow-utils
        - tar
        - unzip
        - util-linux
        - which
        - xz
      srpm-build:
        - bash
        - centos-stream-release
        - centpkg-minimal
        - glibc-minimal-langpack
        - gnupg2
        - redhat-rpm-config
        - rpm-build
        - shadow-utils
    inheritance:
      - parent: "c10s"
        priority: 10

- name: Create the signing buildtools tag
  koji_tag:
    name: c10s-signing-build-tools
    groups:
      build:
        - bash
        - bzip2
        - centos-stream-release
        - centpkg-minimal
        - coreutils
        - cpio
        - diffutils
        - findutils
        - gawk
        - glibc-minimal-langpack
        - grep
        - gzip
        - info
        - make
        - patch
        - redhat-rpm-config
        - rh-dupsign-macros
        - rpm-build
        - sed
        - shadow-utils
        - tar
        - unzip
        - util-linux
        - which
        - xz
      prep-kerberos:
        - krb5-workstation
        - rh-pesign-macros
        - rh-signing-tools-lite
        - rpm-python
      srpm-build:
        - bash
        - centos-stream-release
        - centpkg-minimal
        - glibc-minimal-langpack
        - gnupg2
        - redhat-rpm-config
        - rpm-build
        - shadow-utils
    packages:
      centos-stream:
        - rh-dupsign-macros

- name: Create the toplevel tag
  koji_tag:
    name: "c10s"
    packages:
      centos-stream: "{{ allowlist_pkgs }}"

- name: Create the bootstrap tag
  koji_tag:
    name: "c10s-bootstrap"
    inheritance:
      - parent: "c10s"
        priority: 10

- name: Create the pending tag
  koji_tag:
    name: "c10s-pending"
    inheritance:
      - parent: "c10s"
        priority: 10

- name: Create the build tag
  koji_tag:
    name: "c10s-build"
    arches: "{{ arches }}"
    extra:
      mock.new_chroot: 1
      mock.use_bootstrap: 0
      mock.bootstrap_image: "quay.io/centos/centos:stream10-development"
      mock.package_manager: dnf
      rhpkg_dist: el10
      rpm.macro.dist: '%{!?distprefix0:%{?distprefix}}%{expand:%{lua:for i=0,9999 do print("%{?distprefix" .. i .."}") end}}.el10%{?with_bootstrap:%{__bootstrap}}'
    inheritance:
      - parent: "c10s-build-tools"
        priority: 2
      - parent: "c10s-pending"
        priority: 11
      - parent: "java-openjdk-portable-result" # https://issues.redhat.com/browse/CS-2013
        priority: 17

# workaround to build libkcapi and libxcrypt
- name: Create the build tag
  koji_tag:
    name: "c10s-build-nspawnless"
    arches: "{{ arches }}"
    extra:
      mock.new_chroot: 0 # this is the change
      mock.use_bootstrap: 0
      #mock.bootstrap_image: "quay.io/centos/centos:stream10-development"
      mock.package_manager: dnf
      rhpkg_dist: el10
      rpm.macro.dist: '%{!?distprefix0:%{?distprefix}}%{expand:%{lua:for i=0,9999 do print("%{?distprefix" .. i .."}") end}}.el10%{?with_bootstrap:%{__bootstrap}}'
    inheritance:
      - parent: "c10s-build-tools"
        priority: 2
      - parent: "c10s-pending"
        priority: 11

- name: Create the candidate tag
  koji_tag:
    name: "c10s-candidate"
    inheritance:
      - parent: "c10s"
        priority: 10

- name: Create the override tag
  koji_tag:
    name: "c10s-override"
    inheritance:
      - parent: "c10s"
        priority: 10

- name: Create the released tag
  koji_tag:
    name: "c10s-released"
    inheritance:
      - parent: "c10s-pending"
        priority: 10

- name: Create the production compose tag
  koji_tag:
    name: "c10s-compose"
    inheritance:
      - parent: "c10s-override"
        priority: 3
      - parent: "c10s-pending-signed"
        priority: 5

- name: Create the development compose tag
  koji_tag:
    name: "c10s-compose-development"
    inheritance:
      - parent: "c10s-candidate"
        priority: 5

- name: Create the gate tag
  koji_tag:
    name: "c10s-gate"
    inheritance:
      - parent: "c10s"
        priority: 10

- name: Create the pending-signed tag
  koji_tag:
    name: "c10s-pending-signed"
    inheritance:
      - parent: "c10s"
        priority: 10

- name: Create the draft build tag
  koji_tag:
    name: "c10s-draft"
    inheritance:
      - parent: "c10s"
        priority: 10

# Build targets

- name: Create the candidate target
  koji_target:
    name: "c10s-candidate"
    build_tag: "c10s-build"
    dest_tag: "c10s-gate"

- name: Create the draft target
  koji_target:
    name: "c10s-draft"
    build_tag: "c10s-build"
    dest_tag: "c10s-draft"

- name: Create the build-tools target
  koji_target:
    name: "c10s-build-tools"
    build_tag: "c10s-build"
    dest_tag: "c10s-build-tools"

# workaround to build libkcapi and libxcrypt
- name: Create the candidate target
  koji_target:
    name: "c10s-candidate-nspawnless"
    build_tag: "c10s-build-nspawnless"
    dest_tag: "c10s-gate"


# Image tags

- name: 'make an image base tag' 
  koji_tag:
    name: "guest-c10s"
    packages:
      centos-stream: "{{ image_allowlist_pkgs }}"

- name: 'make an image candidate tag' 
  koji_tag:
    name: 'guest-c10s-candidate'
    inheritance: 
      - parent: 'guest-c10s'
        priority: 10

- name: 'make a container build tag' 
  koji_tag:
    name: 'guest-c10s-container-build'
    arches: "{{ arches }}"
    extra:
      mock.package_manager: dnf
    inheritance: 
      - parent: 'guest-c10s-candidate'
        priority: 10

- name: 'make an image build tag' 
  koji_tag:
    name: 'guest-c10s-image-build'
    arches: "{{ arches }}"
    extra:
      mock.package_manager: dnf
    inheritance: 
      - parent: 'guest-c10s-candidate'
        priority: 10

# Image targets

- name: 'make a container target'
  koji_target:
    name: "c10s-containers"
    build_tag: "guest-c10s-container-build"
    dest_tag: "guest-c10s-candidate"

- name: 'make an image target'
  koji_target:
    name: "c10s-images"
    build_tag: "guest-c10s-image-build"
    dest_tag: "guest-c10s-candidate"

# pesign
- name: 'make a pesign build tag'
  koji_tag:
    state: present
    name: "c10s-pesign-build"
    perm: admin
    arches: "{{ arches }}"
    extra:
      mock.new_chroot: 1
      mock.use_bootstrap: 0
      mock.bootstrap_image: "quay.io/centos/centos:stream10-development"
      mock.package_manager: dnf
      rhpkg_dist: el10
      rpm.macro.dist: '%{!?distprefix0:%{?distprefix}}%{expand:%{lua:for i=0,9999 do print("%{?distprefix" .. i .."}") end}}.el10%{?with_bootstrap:%{__bootstrap}}'
    external_repos:
      - repo: 'c10s-signing'
        priority: 0
    inheritance:
      - parent: "c10s-signing-build-tools"
        priority: 2
      - parent: "c10s-build"
        priority: 5

- name: 'pesign inheritance'
  koji_tag_inheritance:
    parent_tag: "c10s-build"
    child_tag: "c10s-pesign-build"
    priority: 10

- name: pesign default target
  koji_target:
    name: "c10s-candidate-pesign"
    build_tag: "c10s-pesign-build"
    dest_tag: "c10s-gate"

# https://issues.redhat.com/browse/CS-2769
- name: Create the pesign-draft target
  koji_target:
    name: "c10s-pesign-draft"
    build_tag: "c10s-pesign-build"
    dest_tag: "c10s-draft"

# Portable openjdk builds
# (also in other Stream releases)
# https://issues.redhat.com/browse/CS-1618 (original request)
# https://issues.redhat.com/browse/CS-2012 (about adding it for c10s)
- name: "make the java-openjdk-c10s-build tag"
  koji_tag:
    name: java-openjdk-c10s-build
    arches: "{{ arches }}"
    inheritance:
      - parent: java-openjdk-portable-result
        priority: 10
      - parent: c10s-build
        priority: 20

- name: "make the java-openjdk-c9s-build target"
  koji_target:
    name: java-openjdk-c10s-build
    build_tag: java-openjdk-c10s-build
    dest_tag: java-openjdk-c10s-build

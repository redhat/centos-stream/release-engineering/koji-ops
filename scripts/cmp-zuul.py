#! /usr/bin/python3

import os
import sys
import yaml

def _fn2yaml(fname):
    f = open(fname)
    return list(yaml.load_all(f, Loader=yaml.BaseLoader))

def _koji_ops2pkgns():
    y = _fn2yaml("tag-ansible/roles/stream9/vars/main.yml")
    return set(y[0]['allowlist_pkgs'])

def _zuul2pkgns():
    y = _fn2yaml("../project-config/resources/centos-distgits.yaml")
    ret = set()
    for m in y[0]['resources']['projects']['centos-distgits']['source-repositories']:
        for k in m.keys():
            ret.add(os.path.basename(k))
    return ret

s1 = _koji_ops2pkgns()
s2 = _zuul2pkgns()

for s in sorted(s1):
    if s in s2:
        continue
    print("%s not in ZUUL" % s)
for s in sorted(s2):
    if s in s1:
        continue
    print("%s not in Koji-ops" % s)

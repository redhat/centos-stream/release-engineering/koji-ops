#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# SPDX-License-Identifier:      GPL-2.0
# Author: Mohan Boddu <mboddu@redhat.com>



# Script to untag, block and remove the pkg in stream koji when it gets retired.


import koji
import argparse
import yaml

parser = argparse.ArgumentParser()
parser.add_argument('-p','--packages', metavar='package', nargs="+", help='Packages to retire', required=True)
parser.add_argument('-r','--release', metavar='release', help='Release to retire from (c9s, c10s)', required=True)
args = parser.parse_args()
pkgs = args.packages
release = args.release
if release == 'c9s':
    tags = ['c9s-pending', 'c9s-candidate', 'c9s-gate', 'c9s-build']
elif release == 'c10s':
    tags = ['c10s-pending-signed', 'c10s-pending', 'c10s-candidate', 'c10s-gate', 'c10s-build', 'c10s-bootstrap-signed', 'c10s-bootstrap-to-sign', 'c10s-bootstrap']

# Create a koji session
koji_module = koji.get_profile_module("stream")
kojisession = koji_module.ClientSession(koji_module.config.server)

# Log into koji
kojisession.gssapi_login()

for pkg in pkgs:
    for tag in tags:
        builds = kojisession.listTagged(tag, package=pkg)
        for build in builds:
            nvr = build['nvr']
            print('Untagging {0} from {1} tag'.format(nvr, tag))
            kojisession.untagBuild(tag,nvr)
    try:
        print('Blocking {0} from {1} tag'.format(pkg, release))
        kojisession.packageListBlock(release,pkg)
    except:
        print('{0} package is not in {1} tag'.format(pkg, release))
